/**
 * ConfigController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */

module.exports = {
  

  /**
   * `ConfigController.connections()`
   */
  connections: async function (req, res) {
    return res.json({
      todo: 'connections() is not implemented yet!'
    });
  }

};


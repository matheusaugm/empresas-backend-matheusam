/**
 * UserController
 *
 * @description :: Server-side actions for handling incoming requests.
 * @help        :: See https://sailsjs.com/docs/concepts/actions
 */
var jwt = require('jsonwebtoken');
module.exports = {

 
  login: function (req, res) {    
      User.attemptLogin({
      email: req.param('email'),
      password: req.param('password'),
    }, function (err, user) {
      if (err) return res.negotiate(err);
      if (!user) {
        if (req.wantsJSON || !inputs.invalidRedirect) {
          return res.badRequest('Invalid username/password combination.');
        }
        
        return res.view('login');
      }

      var token = jwt.sign({ user: user.id }, sails.config.jwtSecret, { expiresIn: sails.config.jwtExpires });
      return res.ok(token);

    });

  },


    logout: function (req, res) {    
    req.session.me = null;    
    if (req.wantsJSON) {
      return res.ok('Logged out successfully!');
    }    
    return res.redirect('/');
  },
  
};
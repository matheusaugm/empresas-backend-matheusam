/**
 * Enterprise.js
 *
 * @description :: A model definition represents a database table/collection.
 * @docs        :: https://sailsjs.com/docs/concepts/models-and-orm/models
 */

module.exports = {

  attributes: {

    email_ent: {
      type: 'STRING',
      required: false,
      allowNull: true

    },
    phone: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    city: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    country: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    state: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    own: {
      type: 'BOOLEAN',
      required: false,
      allowNull: true
    },
    facebook: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    twitter: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    description: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    name: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
    creator: {
      type: 'STRING',
      required: false,
      allowNull: true
    },
  }
};